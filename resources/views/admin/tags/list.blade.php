@extends('layouts.admin')

@section('content')
    {{--    @include('admin.partials.page-title', ['page_title' => "فهرست کاربران"])--}}

    <div class="card card-flush mt-6 mt-xl-9">
        <!--begin::Card header-->
        <div class="card-header mt-5">
            <!--begin::Card title-->
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1">فهرست تگ ها</h3>
            </div>
            <!--begin::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar my-1">
                <!--begin::Select-->
                <div class="me-6 my-1">
                    <a href="javascript:;" class="btn btn-sm fw-bold btn-primary"
                       data-bs-toggle="modal" data-bs-target="#kt_modal_new_card">
                        ثبت تگ
                    </a>

                    <div class="modal fade" id="kt_modal_new_card" tabindex="-1" style="display: none;"
                         aria-hidden="true">
                        <!--begin::Modal dialog-->
                        <div class="modal-dialog modal-dialog-centered ">
                            <!--begin::Modal content-->
                            <div class="modal-content">
                                <!--begin::Modal header-->
                                <div class="modal-header">
                                    <!--begin::Modal title-->
                                    <h4>افزودن تگ</h4>
                                    <!--end::Modal title-->
                                    <!--begin::Close-->
                                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                        <span class="svg-icon svg-icon-1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                                      transform="rotate(-45 6 17.3137)" fill="currentColor"></rect>
                                                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                                      transform="rotate(45 7.41422 6)" fill="currentColor"></rect>
                                            </svg>
						            	</span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Close-->
                                </div>
                                <!--end::Modal header-->
                                <!--begin::Modal body-->
                                <div class="modal-body scroll-y">
                                    <!--begin::Form-->
                                        <form action="{{ route('admin.tags.store') }}" method="post">
                                            @csrf

                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('admin.__components.label', ['title' => 'عنوان', 'required'=> 1])

                                                    @include('admin.__components.input-text', [
                                                            'name' => 'title',
                                                            'placeholder' => 'عنوان'
                                                             ])
                                                </div>
                                            </div>
                                            <div class="text-center pt-15">
                                                <button type="submit" id="kt_modal_new_card_submit" class="btn btn-sm btn-light-primary">
                                                    <span class="indicator-label">ثبت تگ</span>
                                                    <span class="indicator-progress">Please wait...
									                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                </button>
                                            </div>
                                        </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Modal body-->
                            </div>
                            <!--end::Modal content-->
                        </div>
                        <!--end::Modal dialog-->
                    </div>

                </div>
                <!--end::Select-->
            </div>
            <!--begin::Card toolbar-->
        </div>
        <!--end::Card header-->

        <!--begin::Card body-->
        <div class="card-body py-4">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                <!--begin::Table head-->
                <thead>
                <!--begin::Table row-->
                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                        <th class="min-w-125px">عنوان</th>
                        <th class="min-w-125px">تنظیمات</th>
                    </tr>
                <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="text-gray-600 fw-semibold">
                <!--begin::Table row-->
                @foreach($tags as $tag)
                    <tr>
                        <td>{{ $tag->title }}</td>
                        <td>
                            {{--                            <a href="{{ route('admin.centers.edit', $event) }}" class="badge badge-primary fw-bold">ویرایش</a>--}}
                            {{--                            <a href="{{ route('admin.centers.delete', $event) }}" class="badge badge-danger fw-bold">حذف</a>--}}

                            <a href="javascript:;"
                               class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                               data-inbox="dismiss" data-bs-toggle="modal" data-bs-target="#kt_modal_new_card_{{ $tag->id }}" title="ویرایش">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-user-edit"></i>
                                </span>
                                <!--end::Svg Icon-->
                            </a>

                            <a href="{{ route('admin.tags.delete', $tag) }}"
                               class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger"
                               data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-trash"></i>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                        </td>
                    </tr>

                    <div class="modal fade" id="kt_modal_new_card_{{ $tag->id }}" tabindex="-1" style="display: none;"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered ">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4>ویرایش تگ</h4>

                                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">

                                        <span class="svg-icon svg-icon-1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                                      transform="rotate(-45 6 17.3137)" fill="currentColor">

                                                </rect>
                                                <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                                      transform="rotate(45 7.41422 6)" fill="currentColor">
                                                </rect>
                                            </svg>
						            	</span>

                                    </div>

                                </div>
                                <!--end::Modal header-->
                                <!--begin::Modal body-->
                                <div class="modal-body scroll-y">
                                    <!--begin::Form-->
                                    <form action="{{ route('admin.tags.update', $tag) }}" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-12">
                                                @include('admin.__components.label', ['title' => 'عنوان', 'required'=> 1])

                                                @include('admin.__components.input-text', [
                                                        'name' => 'title',
                                                        'placeholder' => 'عنوان',
                                                        'value' => $tag->title
                                                         ])
                                            </div>
                                        </div>
                                        <div class="text-center pt-15">
                                            <button type="submit" id="kt_modal_new_card_submit" class="btn btn-sm btn-light-primary">
                                                <span class="indicator-label">ثبت تغییرات</span>
                                                <span class="indicator-progress">Please wait...
									                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                            </button>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>

                            </div>
                            <!--end::Modal content-->
                        </div>
                        <!--end::Modal dialog-->
                    </div>

                @endforeach
                <!--end::Table row-->
                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>


@endsection
