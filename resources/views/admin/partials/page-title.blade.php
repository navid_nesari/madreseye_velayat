<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
    <!--begin::Title-->
    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
        {{ isset($page_title) ? $page_title : 'خوش آمدید' }}
    </h1>
    <!--end::Title-->
    <!--begin::Breadcrumb-->
    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
        <!--begin::Item-->
        <li class="breadcrumb-item text-muted">
            <a href="#" class="text-muted text-hover-primary">{{ isset($subtitle) ? $subtitle : 'پنل مدیریت' }}</a>
        </li>
        <!--end::Item-->


    </ul>
    <!--end::Breadcrumb-->
</div>
