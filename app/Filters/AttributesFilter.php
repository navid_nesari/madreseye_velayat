<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;

class AttributesFilter extends QueryFilter
{
    public function title($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('title', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

}
