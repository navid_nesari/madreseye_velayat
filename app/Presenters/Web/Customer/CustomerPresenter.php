<?php


namespace App\Presenters\Web\Customer;
use App\Models\Customer;
use App\Presenters\Contracts\Presenter;

class CustomerPresenter extends Presenter
{

    public function Status()
    {

        if($this->entity->status == "active")
        {
            return "فعال";
        }
        elseif ($this->entity->status == "in-active")
        {
            return "غیر فعال";
        }
    }

}
