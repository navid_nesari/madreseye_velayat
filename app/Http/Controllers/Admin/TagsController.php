<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;
use Ramsey\Uuid\Exception\UnableToBuildUuidException;

class TagsController extends Controller
{

    public function index()
    {
        $tags = Tag::latest()->paginate();

        return view('admin.tags.list', compact('tags'));
    }



    public function store(Request $request)
    {
        // validation
        $request->validate([
            'title' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        // data create
        $data = [
            'title' => $request->input('title')
        ];

        $tag = Tag::create($data);

        if ($tag instanceof Tag) {

        return redirect()->route('admin.tags.list');
        }

        return redirect()->back();
    }



    public function update(Request $request, Tag $tag)
    {
        // validation
        $request->validate([
            'title' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        // data create
        $data = [
            'title' => $request->input('title')
        ];

        $tag->update($data);

        return redirect()->route('admin.tags.list');
    }



    public function delete(Tag $tag)
    {
        $tag->delete();

        return redirect()->back();
    }

}
