<?php


namespace App\Presenters\Web\User;

use App\Constants\Constant;
use App\Models\User;
use App\Presenters\Contracts\Presenter;

class UserPresenter extends Presenter
{
    public function image()
    {
        if (is_null($this->entity->image) || $this->entity->image == "") {
            return asset('admin/src/media/svg/files/blank-image.svg');
        }
        return asset(Constant::USER_IMAGE_PATH . DIRECTORY_SEPARATOR . $this->entity->image);
    }

    public function status()
    {

        if ($this->entity->status == "active") {
            return "فعال";
        } elseif ($this->entity->status == "in-active") {
            return "غیر فعال";
        }
    }
    public function FullNmae()
    {
        return $this->entity->first_name . $this->entity->last_name;
    }

}
