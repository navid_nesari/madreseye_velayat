<?php


namespace App\Presenters\Web\Warehouse;
use App\Constants\Constant;
use App\Models\Warehouse;
use App\Presenters\Contracts\Presenter;

class WarehousePresenter extends Presenter
{

    public function Status()
    {

        if($this->entity->status == "active")
        {
            return "فعال";
        }
        elseif ($this->entity->status == "in-active")
        {
            return "غیر فعال";
        }
    }

}
