p artisan <?php

use App\Http\Controllers\Admin\AdministratorsController;
use App\Http\Controllers\Admin\BlogsController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\CentersController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\TagsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    // administrators
    Route::group(['prefix' => 'administrators'], function () {
        Route::get('/list', [AdministratorsController::class, 'index'])->name('admin.administrators.list');
        Route::get('/create', [AdministratorsController::class, 'create'])->name('admin.administrators.create');
        Route::post('/create', [AdministratorsController::class, 'store'])->name('admin.administrators.store');
        Route::get('/edit/{administrator}', [AdministratorsController::class, 'edit'])->name('admin.administrators.edit');
        Route::post('/update/{administrator}', [AdministratorsController::class, 'update'])->name('admin.administrators.update');
        Route::get('/delete/{administrator}', [AdministratorsController::class, 'delete'])->name('admin.administrators.delete');
    });

    // centers
    Route::group(['prefix' => 'centers'], function () {
        Route::get('/list', [CentersController::class, 'index'])->name('admin.centers.list');
        Route::get('/create', [CentersController::class, 'create'])->name('admin.centers.create');
        Route::post('/create', [CentersController::class, 'store'])->name('admin.centers.store');
        Route::get('/edit/{center}', [CentersController::class, 'edit'])->name('admin.centers.edit');
        Route::post('/update/{center}', [CentersController::class, 'update'])->name('admin.centers.update');
        Route::get('/delete/{center}', [CentersController::class, 'delete'])->name('admin.centers.delete');
    });

    // categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/list', [CategoriesController::class, 'index'])->name('admin.categories.list');
//        Route::get('/create', [CategoriesController::class, 'create'])->name('admin.categories.create');
        Route::post('/create', [CategoriesController::class, 'store'])->name('admin.categories.store');
        Route::get('/edit/{category}', [CategoriesController::class, 'edit'])->name('admin.categories.edit');
        Route::post('/update/{category}', [CategoriesController::class, 'update'])->name('admin.categories.update');
        Route::get('/delete/{category}', [CategoriesController::class, 'delete'])->name('admin.categories.delete');
    });

    // events
    Route::group(['prefix' => 'events'], function () {
        Route::get('/list', [EventsController::class, 'index'])->name('admin.events.list');
        Route::get('/create', [EventsController::class, 'create'])->name('admin.events.create');
        Route::post('/create', [EventsController::class, 'store'])->name('admin.events.store');
        Route::get('/edit/{event}', [EventsController::class, 'edit'])->name('admin.events.edit');
        Route::post('/update/{event}', [EventsController::class, 'update'])->name('admin.events.update');
        Route::get('/delete/{event}', [EventsController::class, 'delete'])->name('admin.events.delete');
        Route::get('ajax-search', [EventsController::class, 'ajax_search'])->name('admin.events.ajax-search');
    });

    // blogs
    Route::group(['prefix' => 'blogs'], function () {
        Route::get('/list', [BlogsController::class, 'index'])->name('admin.blogs.list');
        Route::get('/create', [BlogsController::class, 'create'])->name('admin.blogs.create');
        Route::post('/create', [BlogsController::class, 'store'])->name('admin.blogs.store');
        Route::get('/edit/{blog}', [BlogsController::class, 'edit'])->name('admin.blogs.edit');
        Route::post('/update/{blog}', [BlogsController::class, 'update'])->name('admin.blogs.update');
        Route::get('/delete/{blog}', [BlogsController::class, 'delete'])->name('admin.blogs.delete');
        Route::get('/ajax-search', [BlogsController::class, 'ajax_search'])->name('admin.blogs.ajax-search');
    });

    // tags
    Route::group(['prefix' => 'tags'], function () {
        Route::get('/list', [TagsController::class, 'index'])->name('admin.tags.list');
//        Route::get('/create', [TagsController::class, 'create'])->name('admin.tags.create');
        Route::post('/create', [TagsController::class, 'store'])->name('admin.tags.store');
//        Route::get('/edit/{tag}', [TagsController::class, 'edit'])->name('admin.tags.edit');
        Route::post('/update/{tag}', [TagsController::class, 'update'])->name('admin.tags.update');
        Route::get('/delete/{tag}', [TagsController::class, 'delete'])->name('admin.tags.delete');
    });
});


Route::group(['prefix' => 'auth'], function (){
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('auth.login.form');
    Route::post('/login', [LoginController::class, 'login'])->name('auth.login');
    Route::get('/logout', [LoginController::class, 'logout'])->name('auth.logout');
});





//Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
