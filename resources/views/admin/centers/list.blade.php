@extends('layouts.admin')

@section('content')
    {{--    @include('admin.partials.page-title', ['page_title' => "فهرست کاربران"])--}}

    <div class="card card-flush mt-4">
        <!--begin::Card header-->
        <div class="card-header mt-5">
            <!--begin::Card title-->
            <div class="card-title flex-column">
                <h3 class="fw-bold mb-1">فهرست مراکز</h3>
            </div>
            <!--begin::Card title-->
            <!--begin::Card toolbar-->
            <div class="card-toolbar my-1">
                <!--begin::Select-->
                <div class="me-6 my-1">
                    <a href="{{ route('admin.centers.create') }}" class="btn btn-sm fw-bold btn-primary">
                        ثبت مرکز
                    </a>
                </div>
                <!--end::Select-->

            </div>
            <!--begin::Card toolbar-->
        </div>
        <!--end::Card header-->

        <!--begin::Card body-->
        <div class="card-body py-4">
            <!--begin::Table-->
            <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                <!--begin::Table head-->
                <thead>
                <!--begin::Table row-->
                <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">
                        <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                            <input class="form-check-input" type="checkbox" data-kt-check="true"
                                   data-kt-check-target="#kt_table_users .form-check-input" value="1"/>
                        </div>
                    </th>
                    <th class="min-w-125px">عنوان</th>
                    <th class="min-w-125px">آدرس</th>
                    <th class="min-w-125px">وضعیت</th>
                    <th class="min-w-125px">تنظیمات</th>
                </tr>
                <!--end::Table row-->
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody class="text-gray-600 fw-semibold">
                <!--begin::Table row-->
                @foreach($centers as $center)
                    <tr>
                        <td>
                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" value="1"/>
                            </div>
                        </td>
                        <td>{{$center->title}}</td>
                        <td>{{$center->address}}</td>
                        <td>
                            {!! $center->webPresent()->status !!}
                        </td>
                        <td>
{{--                            <a href="{{ route('admin.centers.edit', $center) }}" class="badge badge-primary fw-bold">ویرایش</a>--}}
{{--                            <a href="{{ route('admin.centers.delete', $center) }}" class="badge badge-danger fw-bold">حذف</a>--}}

                            <a href="{{ route('admin.centers.edit', $center) }}"
                               class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                               data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-user-edit"></i>
                                        </span>
                                <!--end::Svg Icon-->
                            </a>


                            <a href="{{ route('admin.centers.delete', $center) }}"
                               class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger"
                               data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                <!--end::Svg Icon-->
                            </a>
                        </td>
                    </tr>
                @endforeach
                <!--end::Table row-->

                </tbody>
                <!--end::Table body-->
            </table>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>

@endsection
