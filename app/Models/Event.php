<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Event\EventPresenter as webEventPresenter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory, Sluggable, Presentable;

    public $webPresenter = webEventPresenter::class;

    protected $guarded = ['id'];



    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,
            ]
        ];
    }


    public function centers()
    {
        return $this->belongsToMany(Center::class, 'center_events');
    }


    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }


    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
