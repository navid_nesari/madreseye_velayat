<?php

namespace App\Presenters\Web\Blog;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class BlogPresenter extends Presenter
{
    public function image()
    {
        if (is_null($this->entity->image)) {
            return asset('admin-assets/assets/media/avatars/blank.png');
        }

        return str_replace('\\', '/', asset(Constant::BLOG_IMAGE_PATH . $this->entity->image));
    }
}
