<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdministratorsController extends BaseController
{

    public function index()
    {
        $admins = Administrator::latest()->paginate();

        return view('admin.administrators.list', compact('admins'));
    }


    public function create()
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.administrators.create', compact('statuses'));
    }


    public function store(Request $request)
    {
        //validation
        $this->administratorsStoreValidation($request);

        $fileName = null;
        if (! is_null($request->file('avatar'))) {
            $fileName = $this->uploadImage($request->file('avatar'), Constant::ADMIN_IMAGE_PATH);
        }

        $administrator = Administrator::create(array_merge([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'mobile' => $request->input('mobile'),
            'password' => Hash::make($request->input('password')),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'avatar' => $request->file('avatar'),
            'status' => $request->input('status'),
        ], [
            'avatar' => $fileName
        ]));

        if ($administrator instanceof Administrator) {

            return redirect()->route('admin.administrators.list');
        }

        return redirect()->back();
    }



    public function edit(Administrator $administrator)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.administrators.edit', compact('administrator', 'statuses'));
    }



    public function update(Request $request, Administrator $administrator)
    {
        // validation
        $this->administratorsUpdateValidation($request, $administrator);

        $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'mobile' => $request->input('mobile'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'status' => $request->input('status'),
        ];

        if ($request->file('avatar')) {
            $data['avatar'] = $this->uploadImage($request->file('avatar'), Constant::ADMIN_IMAGE_PATH);
        }

        if ($request->has('password')) {
            $data['password'] = Hash::make($request->input('password'));
        }

        $administrator->update($data);

        return redirect()->route('admin.administrators.list');
    }



    public function delete(Administrator $administrator)
    {
        $administrator->delete();

        return redirect()->route('admin.administrators.list');
    }



    private function administratorsStoreValidation($request)
    {
        $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'mobile' => ['required', 'unique:administrators'],
            'password' => ['required', 'min:8', 'confirmed'],
            'email' => ['nullable', 'email', 'unique:administrators'],
            'username' => ['nullable', 'unique:administrators'],
            'avatar' => ['nullable', 'mimes:jpg,png,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)]
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد',
            'mobile.unique' => 'شماره وارد شده باید یکتا باشد',
            'password.min' => 'پسورد وارد شده باید حداقل 8 رقم باشد',
            'password.confirmed' => 'رمز عبور با تکرار آن مطابقت ندارد'
        ]);
    }



    private function administratorsUpdateValidation($request, $administrator)
    {
        $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'mobile' => ['required', Rule::unique('administrators')->ignore($administrator)],
            'password' => ['nullable', 'min:8', 'confirmed'],
            'email' => ['nullable', 'email', Rule::unique('administrators')->ignore($administrator)],
            'username' => ['nullable', Rule::unique('administrators')->ignore($administrator)],
            'avatar' => ['nullable', 'mimes:jpg,png,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)]
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد',
            'mobile.unique' => 'شماره وارد شده باید یکتا باشد',
            'password.min' => 'پسورد وارد شده باید حداقل 8 رقم باشد',
            'password.confirmed' => 'رمز عبور با تکرار آن مطابقت ندارد'
        ]);
    }
}
