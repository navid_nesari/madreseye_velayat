<?php


namespace App\Presenters\Web\Role;


use App\Presenters\Contracts\Presenter;

class RolePresenter extends Presenter
{
    const PUBLIC = 0;
    const ADMIN = 1;
    const SUPER_ADMIN = 2;

    public function permissions()
    {
        if (!is_null($this->entity->permissions)) {
            $data = '';
            foreach ($this->entity->permissions as $key => $permission) {
                $data .= "<span class='badge badge-info ml-1 mb-1'>" . $permission->persian_name . "</span>";
            }
            return $data;

        }
        return "<span class='badge badge-warning font-size-lg'>ندارد</span>";

    }


    public function checkPermission($perm_id)
    {
        $perms = $this->entity->permissions->pluck('id')->toArray();
        return (!in_array($perm_id, $perms)) ?: 'checked';
    }

}
