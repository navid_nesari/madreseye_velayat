<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class BlogsController extends BaseController
{

    public function index()
    {
        $blogs = Blog::latest()->paginate();

        return view('admin.blogs.list', compact('blogs'));
    }



    public function create()
    {
        $statuses = Constant::getBlogStatusesViewer();
        $categories = Category::select(['id', 'title'])->get()->toArray();

        return view('admin.blogs.create', compact('statuses', 'categories'));
    }



    public function store(Request $request)
    {
        // validation
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'image' => ['required', 'mimes:jpg,jpeg,png,bmp'],
            'tags' => ['required'],
            'categories' => ['required']
        ]);

        // data create
        $data = [
            'title' => $request->input('title'),
            'body' => $request->input('body')
        ];

        if ($request->file('image')) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::BLOG_IMAGE_PATH);
        }

        $blog = Blog::create($data);

        if ($blog instanceof Blog) {
            // relations = categoriable
            $blog->categories()->sync($request->input('categories'));

            // relations = taggable
            $blog->tags()->sync($request->input('tags'));

            return redirect()->route('admin.blogs.list');
        }

        return redirect()->back();
    }



    public function edit(Blog $blog)
    {
        $statuses = Constant::getBlogStatusesViewer();
        $categories = Category::select(['id', 'title'])->get()->toArray();

//        $tags = $blog->tags()->get();
        $tags = $blog->tags;
       

        return view('admin.blogs.edit', [
                                'blog' => $blog,
                                'statuses' => $statuses,
                                'categories' => $categories,
                                'tags' => $tags
                            ]);
    }



    public function update(Blog $blog, Request $request)
    {
        // validation
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'image' => ['nullable', 'mimes:jpg,jpeg,png,bmp']
        ]);

        // data update
        $data = [
            'title' => $request->input('title'),
            'body' => $request->input('body')
        ];

        if ($request->file('image')) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::BLOG_IMAGE_PATH);
        }

        $blog->update($data);

        if ($blog instanceof Blog) {
            // relations = categoriable
            $blog->categories()->sync($request->input('categories'));

            // relations = taggable
            $blog->tags()->sync($request->input('tags'));

            return redirect()->route('admin.blogs.list');
        }

        return redirect()->back();
    }



    public function delete(Blog $blog)
    {
        $blog->delete();

        return redirect()->back();
    }



    public function ajax_search()
    {
        $title = request('q');
        $tags = Tag::where('title', 'like', '%' . $title . '%')->get();

        $output = [];
        foreach ($tags as $tag) {
            $output[] = [
                'id' => $tag->id,
                'name' => $tag->title
            ];
        }

        return response()->json(['data' => $output]);
    }
}
