<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;

class WarehousesFilter extends QueryFilter
{
    public function  title ($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('title', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }
    public function number($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('number', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

}
