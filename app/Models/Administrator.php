<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Admin\AdminPresenter as WebAdminPresenter;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Authenticatable
{
    use HasFactory, Presentable;

    public  $webPresenter = WebAdminPresenter::class;

    protected $guarded = [
        'id'
    ];


}
