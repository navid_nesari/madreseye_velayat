<?php

namespace App\Presenters\Web\Event;

use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class EventPresenter extends Presenter
{
    public function status()
    {
        switch ($this->entity->status) {
            case Constant::EVENT_COMING_SOON:
                return "<span class='badge badge-light-warning badge-sm px-2'>به زودی</span>";
                break;
            case Constant::EVENT_REGISTERING:
                return "<span class='badge badge-light-success badge-sm px-2'>در حال ثبت نام</span>";
                break;
            case Constant::EVENT_REGISTRATION_COMPLETE:
                return "<span class='badge badge-light-info badge-sm px-2'>اتمام ثبت نام</span>";
                break;
            case Constant::EVENT_COMPLETION:
                return "<span class='badge badge-light-danger badge-sm px-2'>اتمام رویداد</span>";
                break;
        }
    }


    public function image()
    {
        if( is_null($this->entity->image) ) {
            return asset('admin-assets/assets/media/avatars/blank.png');
        }

        return str_replace('\\', '/', asset(Constant::EVENT_IMAGE_PATH . $this->entity->image));
    }
}
