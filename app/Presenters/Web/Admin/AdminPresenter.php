<?php


namespace App\Presenters\Web\Admin;


use App\Constants\Constant;
use App\Helper\Status;
use App\Models\Administrator;
use App\Presenters\Contracts\Presenter;

class AdminPresenter extends Presenter
{

    public function status()
    {
        if ($this->entity->status == Constant::IN_ACTIVE) {
            return "<span class='badge badge-light-danger badge-sm px-2'>غیر فعال</span>";
        } else {
            return "<span class='badge badge-light-success badge-sm px-2'>فعال</span>";
        }
    }

    public function avatar()
    {

        if ( is_null($this->entity->avatar) ) {
            return asset('admin-assets/assets/media/avatars/blank.png');
        }
        return str_replace('\\', '/', asset(Constant::ADMIN_IMAGE_PATH . $this->entity->avatar));

    }


    public function collaboration()
    {
        return "<label class='badge badge-secondary'> " . $this->entity->getCollaborationType($this->entity->collaboration_type) . " </label>";
    }

    public function roles()
    {
        $roles = '';
        if (!is_null($this->entity->roles)) {
            foreach ($this->entity->roles as $role) {
                $roles .= "<span class='label label-light-info label-inline mb-1 mr-2'>" . $role->persian_name . "</span>";
            }
        }

        return $roles;
    }

    public function file_title()
    {
        return $this->entity->name;
    }

    public function file_slug()
    {
        return str_replace(' ', '-', $this->entity->name);
    }

    public function file_description()
    {
        return '';
    }
}
