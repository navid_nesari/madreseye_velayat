<?php


namespace App\Presenters\Web\File;


use App\Constants\FileConstant;
use App\Constants\GlobalConstant;
use App\Constants\GlobalConstant as GlobalConst;
use App\Helper\Status;
use App\Models\Category;
use App\Models\File;
use App\Presenters\Contracts\Presenter;

class FilePresenter extends Presenter
{

    public function searchTitle()
    {
        return $this->entity->title ;
    }
    public function searchBody()
    {
        return $this->entity->description ;
    }

    public function publishStatus()
    {
        return (!is_null($this->entity->publish_status))
            ?   "<span class='btn btn-sm btn-light-info label-inline mb-1 mr-2'>" . GlobalConst::getPublishStatuses($this->entity->publish_status) . "</span>"
            : "<span class='btn btn-sm btn-light-warning label-inline mb-1 mr-2'>  تعریف نشده  </span>";
    }

    public function categories()
    {
        $categoriesTitle = [];
        $categories = $this->entity->categories;
        if(!is_null($categories)){
            if($categories->count() > 0 ){
                foreach ($categories as $category){
                    $categoriesTitle[] = $category->title;
                }
                return implode(', ', $categoriesTitle);
            }
        }
        return "<span class='btn btn-sm btn-light-danger label-inline mb-1 mr-2'>  ثبت نشده  </span>";

    }
    public function image()
    {
        if(file_exists(FileConstant::FILE_AVATAR_PATH . $this->entity->avatar )){
            return ( !is_null($this->entity->avatar) )
                ? str_replace('\\', '/', asset(FileConstant::FILE_AVATAR_PATH . $this->entity->avatar ))
                :  asset('assets/media/svg/files/blank-image.svg');
        }
        return  asset('assets/media/svg/files/blank-image.svg');

    }

    public function type()
    {
        if(!is_null($this->entity->type) && !in_array($this->entity->type, [
                FileConstant::_IMAGE, FileConstant::_AUDIO, FileConstant::_VIDEO, FileConstant::_TEXT, FileConstant::_PDF,
                FileConstant::_WORD, FileConstant::_POWERPOINT, FileConstant::_EXCEL, FileConstant::_ZIP,
            ])) '<span class="fa fa-question-circle  font-size-h1 text-danger"></span>';


        return (!is_null($this->entity->type))
            ? [
                FileConstant::_IMAGE => '<span class="fa fa-image font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_AUDIO => '<span class="fa fa-file-audio  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_VIDEO => '<span class="fa fa-file-video  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_TEXT => '<span class="fa fa-file-alt  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_PDF => '<span class="fa fa-file-pdf  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_WORD => '<span class="fa fa-file-word  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_POWERPOINT => '<span class="fa fa-file-powerpoint  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_EXCEL => '<span class="fa fa-file-excel  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
                FileConstant::_ZIP => '<span class="fa fa-file-archive  font-size-h1 text-dark-75 text-hover-dark-50"></span>',
            ][$this->entity->type]
            : '<span class="fa fa-question-circle  font-size-h1 text-danger"></span>';
    }

    public function size()
    {
        if ($this->entity->size >= 1073741824)
        {
            $this->entity->size = number_format($this->entity->size / 1073741824, 2) . ' گیگابایت ';
        }
        elseif ($this->entity->size >= 1048576)
        {
            $this->entity->size = number_format($this->entity->size / 1048576, 2) . ' مگابایت ';
        }
        elseif ($this->entity->size >= 1024)
        {
            $this->entity->size = number_format($this->entity->size / 1024, 2) . ' کیلوبایت ';
        }
        elseif ($this->entity->size > 1)
        {
            $this->entity->size = $this->entity->size . ' بایت ';
        }
        elseif ($this->entity->size == 1)
        {
            $this->entity->size = $this->entity->size . ' بایت ';
        }
        else
        {
            $this->entity->size = '0 بایت ';
        }

        return $this->entity->size;
    }

    public function fileUrl()
    {
        if (in_array($this->entity->type, [FileConstant::_VIDEO])){
            return asset(FileConstant::FILE_VIDEO_PATH . $this->entity->name);
        }elseif (in_array( $this->entity->type ,[FileConstant::_AUDIO])){
            return asset(FileConstant::FILE_AUDIO_PATH . $this->entity->name);
        }elseif (in_array( $this->entity->type ,[FileConstant::_IMAGE])){
            return asset(FileConstant::FILE_IMAGE_PATH . $this->entity->name);
        }elseif (in_array( $this->entity->type ,[FileConstant::_TEXT, FileConstant::_PDF, FileConstant::_WORD, FileConstant::_POWERPOINT, FileConstant::_EXCEL, FileConstant::_ZIP])) {
            return asset(FileConstant::FILE_OTHER_PATH . $this->entity->name);
        }else{
            return '';
        }
    }


    public function video()
    {
        return (!is_null($this->entity->organizerFiles) && count($this->entity->organizerFiles) > 0)
            ? $this->entity->organizerFiles()->where('files.type', FileConstant::_VIDEO)
            : '';
    }
    public function videoUrl()
    {
        if (!is_null($this->entity->organizerFiles)){
            $video = $this->entity->organizerFiles(function ($query){
                $query->where('files.type', FileConstant::_VIDEO);
            })->first();
            if($video instanceof File){
                $url = asset(FileConstant::ORGANIZER_VIDEO_PATH . $video->name );
                return str_replace('\\', '/', $url);

            }
            return null;
        }
        return null;
    }





}
