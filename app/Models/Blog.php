<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Blog\BlogPresenter as webBlogPresenter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory, Sluggable, Presentable;

    protected $webPresenter = webBlogPresenter::class;

    protected $guarded = ['id'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,
            ]
        ];
    }


    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }


    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
