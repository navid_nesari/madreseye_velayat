<?php


namespace App\Presenters\Web\Product;


use App\Constants\Constant;
use App\Constants\GlobalConstant;
use App\Models\Administrator;
use App\Presenters\Contracts\Presenter;

class ProductPresenter extends Presenter
{

    public function image()
    {
        if (is_null($this->entity->image) || $this->entity->image == "") {
            return asset('admin/src/media/svg/files/blank-image.svg');
        }
        return asset(Constant::PRODUCT_IMAGE_PATH . DIRECTORY_SEPARATOR . $this->entity->image);
    }


    public function collaboration()
    {
        return "<label class='badge badge-secondary'> " . $this->entity->getCollaborationType($this->entity->collaboration_type) . " </label>";
    }



    public function price()
    {
        if (is_null($this->entity->price) || $this->entity->price == "" || $this->entity->price == 0) {
            return "<span class='badge badge-light-warning  mb-1 mr-2'>  نا مشخص </span>";
        }
        return number_format($this->entity->price) . ' تومان ';
    }

    public function sku()
    {
        return (!is_null($this->entity->sku))
            ? "<span class='badge badge-light-info  mb-1 mr-2'>" . $this->entity->sku . "</span>"
            : "<span class='badge badge-light-warning  mb-1 mr-2'>  ندارد  </span>";
    }
    public function category()
    {

        return implode(',', $this->entity->categories->pluck('title')->toArray());
    }
    public function Status()
    {

        if ($this->entity->status == "exists") {
            return "موچود";
        } elseif ($this->entity->status == "not-exists") {
            return " نا موجود";
        }
    }

}
