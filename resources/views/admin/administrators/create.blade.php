@extends('layouts.admin')

@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form id="kt_ecommerce_add_category_form"
                  class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
                  data-kt-redirect="" action="{{ route('admin.administrators.store') }}" method="post"
                  enctype="multipart/form-data">
            @csrf
            <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h2>افزودن مدیر</h2>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{ route('admin.administrators.list') }}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'نام', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'first_name',
                                            'placeholder' => 'نام',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'نام خانوادگی', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'last_name',
                                            'placeholder' => 'نام خانوادگی',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label', ['title' => 'شماره موبایل', 'required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'mobile',
                                            'placeholder' => 'شماره موبایل'
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'رمز عبور','required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'password',
                                            'type' => 'password',
                                            'placeholder' => 'رمز عبور',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'تکرار رمز عبور','required' => 1])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'password_confirmation',
                                            'type' => 'password',
                                            'placeholder' => 'تکرار رمز عبور',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'ایمیل'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'email',
                                            'type' => 'email',
                                            'placeholder' => 'ایمیل',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                    @include('admin.__components.label',['title' => 'نام کاربری'])
                                    <!--end::Label-->
                                        <!--begin::Input-->
                                    @include('admin.__components.input-text', [
                                            'name' => 'username',
                                            'placeholder' => 'نام کاربری',
                                            ])
                                    <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--begin::Input group-->


                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <!--begin::Automation-->
                    <!--end::Automation-->
                </div>
                <!--end::Main column-->
                <!--begin::Aside column-->
                <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                    <!--begin::Status-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>وضعیت</h4>
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar">
                                <div class="rounded-circle bg-success w-15px h-15px"
                                     id="kt_ecommerce_add_category_status"></div>
                            </div>
                            <!--begin::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Select2-->
                            <div class="mb-10 fv-row fv-plugins-icon-container">

                                @include('admin.__components.horizontal-radiobutton',[
                                        'items' => $statuses,
                                        'name' => 'status',
                                        'activeKey' => \App\Constants\Constant::ACTIVE,
                                ])

                            </div>

                            <!--end::Datepicker-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Status-->
                    <!--begin::Thumbnail settings-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h4>تصویر</h4>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body text-center pt-0">
                            <div class="mb-10 fv-row fv-plugins-icon-container">

                                @include('admin.__components.image-input',[
                                    'name' => 'avatar',
                                    ])

                            </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <div class="d-flex justify-content-end">
                        <!--begin::Button-->
                        <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت مدیر</span>

                        </button>
                        <!--end::Button-->
                    </div>
                    <!--end::Thumbnail settings-->
                </div>

            </form>

        </div>
    </div>
@endsection
