<?php


namespace App\Presenters\Web\Permission;


use App\Presenters\Contracts\Presenter;

class PermissionPresenter extends Presenter
{

    public function group()
    {
         return (!is_null($this->entity->group) )
            ? "<span class='badge badge-secondary font-size-lg'>".$this->entity->permissionsGroup()."</span>"
            : "<span class='badge badge-warning font-size-lg'>عنوان گروه : " . $this->entity->persian_name . "</span>";

    }

}
