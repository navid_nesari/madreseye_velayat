<?php


namespace App\Filters;
use App\Filters\Contracts\QueryFilter;

class AttributeGroupsFilter extends QueryFilter
{
    public function name($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('name', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

}
