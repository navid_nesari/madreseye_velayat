<?php


namespace App\Filters;


use App\Constants\GlobalConstant as GlobalConst;
use App\Filters\Contracts\QueryFilter;

class CategoriesFilter extends QueryFilter
{
    public function title($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('title', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }

}
