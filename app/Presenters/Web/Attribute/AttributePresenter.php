<?php


namespace App\Presenters\Web\Attribute;

use App\Constants\Constant;
use App\Models\Attribute;
use App\Models\AttributeGroupe;
use App\Models\AttributeItem;
use App\Models\Category;
use App\Presenters\Contracts\Presenter;

class AttributePresenter extends Presenter
{









    public function category($category_id)
    {

        $value = Category::where('id', $category_id)->first();
        return $value->title;

    }

    public function attribute_group()
    {

        $attribute_group_id = $this->entity->attribute_groupe_id;
        $attribute_group = AttributeGroupe::where('id', $attribute_group_id)->first();
        return $attribute_group->name;
    }

    public function attributeItems()
    {

        return implode(',', $this->entity->AttributeItems->pluck('value')->toArray());

    }
}
