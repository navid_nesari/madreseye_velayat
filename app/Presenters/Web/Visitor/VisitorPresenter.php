<?php


namespace App\Presenters\Web\Visitor;
use App\Constants\Constant;
use App\Models\Company;
use App\Presenters\Contracts\Presenter;

class VisitorPresenter extends Presenter
{

    public function image()
    {
        if(is_null($this->entity->image) || $this->entity->image == "") {
            return asset('admin/src/media/svg/files/blank-image.svg');
        }
        return asset(Constant::Visitor_IMAGE_PATH . DIRECTORY_SEPARATOR . $this->entity->image);
    }

    public function Status()
    {

        if($this->entity->status == "active")
        {
            return "فعال";
        }
        elseif ($this->entity->status == "in-active")
        {
            return "غیر فعال";
        }
    }
}
