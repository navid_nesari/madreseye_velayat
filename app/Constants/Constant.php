<?php


namespace App\Constants;

class Constant
{

    // Global Constant
    const UN_DEFINED = "مشخص نیست !";

    const ACTIVE = 'active';
    const IN_ACTIVE = 'in-active';

    const MALE = 'male';
    const FEMALE = 'female';
    const PENDING = 'pending';
    const CONFIRMED = 'confirm';
    const REJECTED = 'rejected';

    const DRAFT = 'draft';
    const SCHEDULED = 'scheduled';

    const DELETED = 'deleted';
    const UN_DELETED = 'un-deleted';


    const SEARCHABLE = true;

    // File Constant
    const WHITE_MIME_TYPE_LIST = [
        'image/jpeg', 'image/png', 'image/jpg', 'audio/mpeg', 'video/mp4',
        'application/zip', 'application/vnd.ms-powerpoint',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.rar', 'text/plain', 'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];


    const PRODUCT_AVATAR_SIZE = [
        '100' => ['w' => '100', 'h' => '100'],
        '300' => ['w' => '300', 'h' => '300']
    ];
    const USER_AVATAR_SIZE = [
        '100' => ['w' => '200', 'h' => '200'],
    ];

    //admin
    const ADMIN_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'admins' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';

    //center
    const CENTER_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'centers' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';


    // categories
    const CATEGORY_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'categories' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';
    const CATEGORY_EVENT = 'event';
    const CATEGORY_BLOG = 'blog';


    // events
    const EVENT_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';
    const EVENT_COMING_SOON = 'coming-soon';
    const EVENT_REGISTERING = 'registering';
    const EVENT_REGISTRATION_COMPLETE = 'register-complete';
    const EVENT_COMPLETION = 'complete';


    // blogs
    const BLOG_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'blogs' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';
    const PUBLISHED = 'published';
    const UNPUBLISHED = 'unpublished';

    //user
    const USER_IMAGE_PATH = 'uploads' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . '';


    public static function getStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::ACTIVE => 'فعال',
                Constant::IN_ACTIVE => ' غیر فعال',
            ];
        }
        if (in_array($status, array_keys(self::getStatuses()))) {
            return self::getStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }


    public static function getStatusesViewer()
    {
        $activeStatuses = self::getStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }


    public static function getEntities($entity = null)
    {
        if (is_null($entity)) {
            return [
              Constant::CATEGORY_EVENT => 'رویداد',
              Constant::CATEGORY_BLOG => 'مقاله',
            ];
        }

        if (in_array($entity, array_keys(self::getEntities()))) {
            return self::getEntities()[$entity];
        }

        return Constant::UN_DEFINED;
    }


    public static function getEntitiesViewer()
    {
        $activeEntities = self::getEntities();
        $activeEntitiesViewer = [];
        foreach ($activeEntities as $key => $value) {
            $activeEntitiesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeEntitiesViewer;
    }


    public static function getEventStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant:: EVENT_COMING_SOON => 'به زودی',
                Constant::EVENT_REGISTERING => 'در حال ثبت نام',
                Constant::EVENT_REGISTRATION_COMPLETE => 'پایان ثبت نام',
                Constant::EVENT_COMPLETION => 'اتمام رویداد',
            ];
        }
        if (in_array($status, array_keys(self::getEventStatuses()))) {
            return self::getEventStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }


    public static function getEventStatusesViewer()
    {
        $activeStatuses = self::getEventStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }


    public static function getBlogStatuses($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::PUBLISHED => 'منتشر شده',
                Constant::UNPUBLISHED => 'منتشر نشده',
            ];
        }
        if (in_array($status, array_keys(self::getBlogStatuses()))) {
            return self::getBlogStatuses()[$status];
        }
        return Constant::UN_DEFINED;
    }


    public static function getBlogStatusesViewer()
    {
        $activeStatuses = self::getBlogStatuses();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }











    public static function getGenders($status = null)
    {
        if (is_null($status)) {
            return [
                Constant::MALE => 'مرد',
                Constant::FEMALE => ' زن',
            ];
        }
        if (in_array($status, array_keys(self::getGenders()))) {
            return self::getGenders()[$status];
        }
        return Constant::UN_DEFINED;
    }
    public static function getGenderViewer()
    {
        $activeStatuses = self::getGenders();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }


    public static function getTypes($status = null)
    {
        if (is_null($status)) {
            return [
//                Constant::Entry => 'ورود',
//                Constant::Exit => '  خروج',
            ];
        }
        if (in_array($status, array_keys(self::getTypes()))) {
            return self::getTypes()[$status];
        }
        return Constant::UN_DEFINED;
    }
    public static function getTypesViewer()
    {
        $activeStatuses = self::getTypes();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }

    public static function getExists($status = null)
    {
        if (is_null($status)) {
            return [
//                Constant::EXISTS => 'موچود',
//                Constant::NOT_EXISTS => '  ناموجود',
            ];
        }
        if (in_array($status, array_keys(self::getExists()))) {
            return self::getExists()[$status];
        }
        return Constant::UN_DEFINED;
    }
    public static function getExistsViewer()
    {
        $activeStatuses = self::getExists();
        $activeStatusesViewer = [];
        foreach ($activeStatuses as $key => $value) {
            $activeStatusesViewer[] = [
                'id' => $key,
                'title' => $value
            ];
        }
        return $activeStatusesViewer;
    }
}
