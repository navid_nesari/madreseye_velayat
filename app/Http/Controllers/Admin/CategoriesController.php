<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends BaseController
{

    public function index()
    {
        $categories = Category::latest()->paginate();
        $statuses = Constant::getStatusesViewer();
        $entities = Constant::getEntitiesViewer();

        return view('admin.categories.list', [
            'categories' => $categories,
            'statuses' => $statuses,
            'entities' => $entities
        ]);
    }



//    public function create()
//    {
//        $statuses = Constant::getStatusesViewer();
//
//        return view('admin.categories.create', compact('statuses'));
//    }



    public function store(Request $request)
    {
        // validation
        $request->validate([
           'title' => ['required'],
           'image' => ['nullable', 'mimes:jpg,png,jpeg,bmp'],
           'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        // data create in database
        $data =[
          'title' => $request->input('title'),
          'entity' => Constant::CATEGORY_EVENT,
          'status' => $request->input('status'),
        ];

        if ( $request->file('image') ) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::CATEGORY_IMAGE_PATH);
        }

        $category = Category::create($data);

        // return to categories list
        if($category instanceof Category) {
            return redirect()->route('admin.categories.list');
        }

        return redirect()->back();
    }



    public function edit(Category $category)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.categories.edit', [
            'category' => $category,
            'statuses' => $statuses,
            ]);
    }



    public function update(Request $request, Category $category)
    {
        // validation
        $request->validate([
            'title' => ['required'],
            'image' => ['nullable', 'mimes:jpg,png,jpeg,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        // data update in database
        $data =[
            'title' => $request->input('title'),
            'status' => $request->input('status'),
        ];

        if ( $request->file('image') ) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::CATEGORY_IMAGE_PATH);
        }

        $category->update($data);

        return redirect()->route('admin.categories.list');
    }



    public function delete(Category $category)
    {
        $category->delete();

        return redirect()->back();
    }
}
