<?php


namespace App\Presenters\Web\AttributeGroupe;
use App\Constants\Constant;
use App\Models\AttributeGroupe;
use App\Presenters\Contracts\Presenter;

class AttributeGroupePresenter extends Presenter
{

    public function Status()
    {

        if($this->entity->status == "active")
        {
            return "فعال";
        }
        elseif ($this->entity->status == "in-active")
        {
            return "غیر فعال";
        }
    }

}
