<?php


namespace App\Filters;

use App\Filters\Contracts\QueryFilter;

class CompaniesFilter extends QueryFilter
{
    public function name($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('name', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }
    public function phone($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('phone', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }
}
