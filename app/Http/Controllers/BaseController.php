<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class BaseController extends Controller
{
    public function uploadImage(UploadedFile $file, string $path )
    {
        // generate unique name for file
        $file_name = now()->timestamp . '-' . $file->getClientOriginalName();


        // check directory exist or not
        if (! File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        // upload file
        $file->move(public_path($path), $file_name);

        // return file_name ( not file path ! )
        return $file_name;
    }
}
