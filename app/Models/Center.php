<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Center\CenterPresenter as webCenterPresenter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    use HasFactory, Sluggable, Presentable;

    public  $webPresenter = webCenterPresenter::class;

    protected $guarded = ['id'];


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,
            ]
        ];
    }


    public function events()
    {
        return $this->belongsToMany(Event::class);
    }
}
