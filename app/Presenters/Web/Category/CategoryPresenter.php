<?php


namespace App\Presenters\Web\Category;


use App\Constants\CategoryConstant;
use App\Constants\Constant;
use App\Presenters\Contracts\Presenter;

class CategoryPresenter extends Presenter
{

    public function status()
    {
        if ($this->entity->status == Constant::IN_ACTIVE) {
            return "<span class='badge badge-light-danger badge-sm px-2'>غیر فعال</span>";
        } else {
            return "<span class='badge badge-light-success badge-sm px-2'>فعال</span>";
        }
    }


    public function image()
    {
        if( is_null($this->entity->image) ) {
            return asset('admin-assets/assets/media/avatars/blank.png');
        }

        return str_replace('\\', '/', asset(Constant::CATEGORY_IMAGE_PATH . $this->entity->image));
    }





    public function attributeGroup()
    {

        return implode(',', $this->entity->attributegroups->pluck('name')->toArray());
    }
    public function parent()
    {
        return (!is_null($this->entity->parent_id))
            ?   "<span class='badge badge-light-info  mb-1 mr-2'>" . $this->entity->parent->title . "</span>"
            : "<span class='badge badge-light-warning  mb-1 mr-2'>  ندارد  </span>";
    }


    public function isSearchable()
    {
        return (!is_null($this->entity->is_searchable)&& $this->entity->is_searchable)
            ?   "<span class='badge badge-light-info mb-1 mr-2'>بله</span>"
            : "<span class='badge badge-light-warning  mb-1 mr-2'>  خیر  </span>";
    }
//    public function image()
//    {
//        if(is_null($this->entity->image) || $this->entity->image == "") {
//            return asset('admin/src/media/svg/files/blank-image.svg');
//        }
//        return asset(Constant::CATEGORY_IMAGE_PATH . DIRECTORY_SEPARATOR . $this->entity->image);
//    }
}
