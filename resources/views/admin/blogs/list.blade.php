@extends('layouts.admin')

@section('content')
    {{--    @include('admin.partials.page-title', ['page_title' => "فهرست کاربران"])--}}

    <div class="row mt-4">

        <div class="card card-flush">
            <!--begin::Card header-->
            <div class="card-header mt-5">
                <!--begin::Card title-->
                <div class="card-title flex-column">
                    <h3 class="fw-bold mb-1">فهرست مقالات</h3>
                    <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                         تعداد کل : <i class="badge badge-sm badge-light-warning px-2" style="font-size: 10px">{{ count($blogs) }}</i>
                    </span>
                </div>

                <div class="card-toolbar my-1">
                    <!--begin::Select-->
                    <div class="me-6 my-1">
                        <a href="{{ route('admin.blogs.create') }}" class="btn btn-sm fw-bold btn-primary">
                            ثبت مقاله
                        </a>
                    </div>
                    <!--end::Select-->
                </div>
                <!--begin::Card title-->
            </div>
            <!--end::Card header-->

            <!--begin::Card body-->
            <div class="card-body py-4">
                <!--begin::Table-->
                <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                    <!--begin::Table head-->
                    <thead>

                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                        <th class="min-w-125px">تصویر</th>
                        <th class="min-w-125px">عنوان</th>
                        <th class="min-w-125px">تعداد نظرات</th>
                        <th class="min-w-125px">تعداد بازدید</th>
                        <th class="min-w-125px">تنظیمات</th>
                    </tr>

                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody class="text-gray-600 fw-semibold">
                    <!--begin::Table row-->
                    @foreach($blogs as $blog)
                        <tr>
                            <td><img src="{{ $blog->webPresent()->image }}" class="img-fluid" width="40"></td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->comment_count }}</td>
                            <td>{{ $blog->view_count }}</td>
                            <td>
                                {{--                            <a href="{{ route('admin.centers.edit', $event) }}" class="badge badge-primary fw-bold">ویرایش</a>--}}
                                {{--                            <a href="{{ route('admin.centers.delete', $event) }}" class="badge badge-danger fw-bold">حذف</a>--}}

                                <a href="{{ route('admin.blogs.edit', $blog) }}"
                                   class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                   data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                    <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-user-edit"></i>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>

                                <a href="{{ route('admin.blogs.delete', $blog) }}"
                                   class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger"
                                   data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                    <span class="svg-icon svg-icon-2 p-1">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                    <!--end::Svg Icon-->
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <!--end::Table row-->

                    </tbody>
                    <!--end::Table body-->
                </table>
                <!--end::Table-->

                {{--                    {!! $blogs->render !!}--}}
            </div>
            <!--end::Card body-->
        </div>
    </div>

@endsection
