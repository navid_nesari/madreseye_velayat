<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Center;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CentersController extends BaseController
{

    public function index()
    {
        $centers = Center::latest()->paginate();

        return view('admin.centers.list', compact('centers'));
    }



    public function create()
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.centers.create', compact('statuses'));
    }



    public function store(Request $request)
    {
        // validation
        $this->centersStoreValidation($request);

        // image upload
        $fileName = null;
        if (! is_null($request->file('logo'))) {
            $fileName = $this->uploadImage($request->file('logo'), Constant::CENTER_IMAGE_PATH);
        }

        // data create in database
        $center = Center::create(array_merge([
            'title' => $request->input('title'),
            'address' => $request->input('address'),
            'description' => $request->input('description'),
            'logo' => $request->file('logo'),
            'status' => $request->input('status')
        ],[
            'logo' => $fileName
        ]));

        if ($center instanceof Center) {
            return redirect()->route('admin.centers.list');
        }

        return redirect()->back();
    }



    public function edit(Center $center)
    {
        $statuses = Constant::getStatusesViewer();

        return view('admin.centers.edit', compact('center', 'statuses'));
    }



    public function update(Request $request, Center $center)
    {
        // validation
        $this->centersUpdateValidation($request);

        $data = [
            'title' => $request->input('title'),
            'address' => $request->input('address'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ];

        // image upload
        if ( $request->file('logo') ) {
            $data['logo'] = $this->uploadImage($request->file('logo'), Constant::CENTER_IMAGE_PATH);
        }

        // data update in database
        $center->update($data);

        return redirect()->route('admin.centers.list');
    }



    public function delete(Center $center)
    {
        $center->delete();

        return redirect()->back();
    }



    private function centersStoreValidation($request)
    {
        $request->validate([
            'title' => ['required'],
            'address' => ['nullable'],
            'description' => ['nullable'],
            'logo' => ['nullable', 'mimes:jpg,png,bmp,jpeg'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)]
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد',
        ]);
    }


    public function centersUpdateValidation($request)
    {
        $request->validate([
            'title' => ['required'],
            'address' => ['nullable'],
            'description' => ['nullable'],
            'logo' => ['nullable', 'mimes:jpg,png,bmp'],
            'status' => ['required', Rule::in(Constant::ACTIVE, Constant::IN_ACTIVE)]
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد',
        ]);
    }
}
