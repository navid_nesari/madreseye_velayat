<?php


namespace App\Filters;


use App\Constants\Constant;
use App\Constants\GlobalConstant as GlobalConst;
use App\Filters\Contracts\QueryFilter;

class ProductsFilter extends QueryFilter
{
    public function title($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('title', 'like', '%'.$value.'%');
        }
        return $this->builder;
    }
    public function sort($value = null)
    {

        switch ($value) {
            case  "new-product":
                if(!is_null($value)){
                    return $this->builder->orderBy('id', 'DESC');
                }
                break;
            case "min-price":
                if(!is_null($value)){
                    return $this->builder->orderBy('price', 'ASC');
                }
                break;
            case  "max-price":
                if(!is_null($value)){
                    return $this->builder->orderBy('price', 'DESC');
                }
                break;
        }


        dd($value);

    }
    public function exists($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('status',Constant::EXISTS);
        }
        return $this->builder;
    }
    public function min_price($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('price' ,'>=' ,$value);
        }
        return $this->builder;
    }
    public function max_price($value = null)
    {
        if(!is_null($value)){
            return $this->builder->where('price' ,'<=' ,$value);
        }
        return $this->builder;
    }
}
