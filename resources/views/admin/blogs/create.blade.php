@extends('layouts.admin')

@section('content')

    <div id="kt_content_container" class="container-xxl">

        <div class="row">
            <div class="mt-4">
                <form id="kt_ecommerce_add_category_form"
                      class="form d-flex flex-column flex-lg-row fv-plugins-bootstrap5 fv-plugins-framework"
                      data-kt-redirect="" action="{{ route('admin.blogs.store') }}" method="post"
                      enctype="multipart/form-data">
                @csrf
                <!--begin::Main column-->
                    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10" style="margin-left: 20px ">
                        <!--begin::General options-->
                        <div class="card card-flush py-4">
                            <!--begin::Card header-->
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>افزودن مقاله</h2>
                                </div>
                                <div class="card-toolbar">
                                    <a href="{{ route('admin.blogs.list') }}" class="btn btn-sm btn-light-success "
                                       style="margin-left: 5px">
                                        بازگشت
                                    </a>

                                </div>
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0 mt-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                            @include('admin.__components.label', ['title' => 'عنوان', 'required' => 1])
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            @include('admin.__components.input-text', [
                                                    'name' => 'title',
                                                    'placeholder' => 'عنوان',
                                                    ])
                                            <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'تگ', 'required' => 1])
                                        <!--end::Label-->
                                            <!--begin::Input-->
                                        @include('admin.__components.select-2-ajax', [
                                                'name' => 'tags',
                                                'isMultiple' => true,
                                                'url' =>route('admin.blogs.ajax-search'),
                                                ])
                                        <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'دسته بندی های مقاله', 'required' => 1])
                                        <!--end::Label-->
                                            <!--begin::Input-->
                                        @include('admin.__components.select-2', [
                                                'name' => 'categories',
                                                'isMultiple' => true,
                                                'items' => $categories,
                                                ])
                                        <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">

                                            @include('admin.__components.label', ['title' => 'گالری', 'required' => 1])
                                            <input class="form-control" type="file" id="formFileMultiple" multiple />

                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                            @include('admin.__components.label', ['title' => 'توضیحات', 'required' => 1])
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            @include('admin.__components.ckeditor', [
                                                    'name' => 'body',
                                                    ])
                                            <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--begin::Input group-->
                            </div>
                            <!--end::Card header-->
                        </div>
                        <!--end::General options-->
                        <!--begin::Automation-->
                        <!--end::Automation-->
                    </div>
                    <!--end::Main column-->
                    <!--begin::Aside column-->
                    <div class="d-flex flex-column gap-7 gap-lg-10 w-100 w-lg-300px mb-7 me-lg-10">
                        <!--begin::Status-->
                        <div class="card card-flush py-4">
                            <!--begin::Card header-->
                            <div class="card-header">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <h4>وضعیت</h4>
                                </div>
                                <!--end::Card title-->
                                <!--begin::Card toolbar-->
                                <div class="card-toolbar">
                                    <div class="rounded-circle bg-success w-15px h-15px"
                                         id="kt_ecommerce_add_category_status"></div>
                                </div>
                                <!--begin::Card toolbar-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0">
                                <!--begin::Select2-->
                                <div class="mb-10 fv-row fv-plugins-icon-container">
                                    @include('admin.__components.vertical-radiobutton',[
                                            'name' => 'status',
                                            'items' => $statuses,
                                    ])
                                </div>
                                <!--end::Datepicker-->
                            </div>
                            <!--end::Card body-->
                        </div>
                        <div class="card card-flush py-4">
                            <!--begin::Card header-->
                            <div class="card-header">
                                <!--begin::Card title-->
                                <div class="card-title">
                                    <h4>تصویر</h4>
                                </div>
                                <!--end::Card title-->
                            </div>

                            <div class="card-body text-center pt-0">
                                <div class="mb-10 fv-row fv-plugins-icon-container">

                                    @include('admin.__components.image-input',[
                                        'name' => 'image',
                                        ])

                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <!--begin::Button-->
                            <button type="submit" id="kt_ecommerce_add_category_submit" class="btn btn-primary">
                                <span class="indicator-label">ثبت مقاله</span>

                            </button>
                            <!--end::Button-->
                        </div>
                        <!--end::Thumbnail settings-->
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

