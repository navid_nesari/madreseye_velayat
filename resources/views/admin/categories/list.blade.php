@extends('layouts.admin')

@section('content')
    {{--    @include('admin.partials.page-title', ['page_title' => "فهرست کاربران"])--}}

    <div class="row mt-4">
        <div class="col-md-4">
            <!--begin::Main column-->
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('admin.categories.store') }}" method="post"
                          enctype="multipart/form-data">
                    @csrf
                    <!--begin::General options-->
                        <div class="card card-flush py-4">
                            <!--begin::Card header-->
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>افزودن دسته بندی</h2>
                                </div>
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body pt-0 mt-2">

                                <div class="row">
                                    <div class="col-md-7">
                                        @include('admin.__components.label', ['title' => 'تصویر', 'required' => 1])
                                    </div>
                                    <div class="col-md-5">
                                        @include('admin.__components.image-input',[
                                               'name' => 'image',
                                               ])
                                    </div>
                                    <div class="col-md-12">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                            @include('admin.__components.label', ['title' => 'عنوان', 'required' => 1])

                                            @include('admin.__components.input-text', [
                                                    'name' => 'title',
                                                    'placeholder' => 'عنوان',
                                                    ])
                                            <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="mb-10 fv-row fv-plugins-icon-container">
                                            <!--begin::Label-->
                                            @include('admin.__components.label', ['title' => 'بخش مربوطه', 'required' => 1])

                                            @include('admin.__components.select-2', [
                                                    'name' => 'entities',
                                                    'items' => $entities,
                                                    ])
                                            <!--end::Input-->
                                            <div class="fv-plugins-message-container invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <!--begin::Label-->
                                        @include('admin.__components.label', ['title' => 'وضعیت', 'required' => 1])
                                        <!--end::Label-->
                                    </div>
                                    <div class="col-md-7">
                                        <!--begin::Input-->
                                        @include('admin.__components.horizontal-radiobutton',[
                                                   'items' => $statuses,
                                                   'name' => 'status',
                                                   'activeKey' => \App\Constants\Constant::ACTIVE,
                                           ])
                                        <!--end::Input-->
                                    </div>

                                </div>

                                <!--begin::Input group-->
                            </div>
                            <!--end::Card header-->

                            <div class="card-footer">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-sm btn-light-success">
                                        <i class="fa fa-plus"></i>
                                        ثبت دسته بندی
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--end::General options-->
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card card-flush ">
                <!--begin::Card header-->
                <div class="card-header mt-5">
                    <!--begin::Card title-->
                    <div class="card-title flex-column">
                        <h3 class="fw-bold mb-1">فهرست دسته بندی ها</h3>
                        <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                             تعداد کل : <i class="badge badge-sm badge-light-warning px-2" style="font-size: 10px">{{ count($categories) }}</i>
                        </span>
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->

                <!--begin::Card body-->
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                        <!--begin::Table head-->
                        <thead>
                        <!--begin::Table row-->
                        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                            <th class="min-w-125px">تصویر</th>
                            <th class="min-w-125px">عنوان</th>
                            <th class="min-w-125px">نوع</th>
                            <th class="min-w-125px">وضعیت</th>
                            <th class="min-w-125px">تنظیمات</th>
                        </tr>
                        <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="text-gray-600 fw-semibold">
                        <!--begin::Table row-->
                        @foreach($categories as $category)
                            <tr>
                                <td><img src="{{ $category->webPresent()->image }}" class="img-fluid" width="40"></td>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->entity }}</td>
                                <td>{!! $category->webPresent()->status !!}</td>
                                <td>
                                    {{--                            <a href="{{ route('admin.categories.edit', $category) }}" class="badge badge-primary fw-bold">ویرایش</a>--}}
                                    {{--                            <a href="{{ route('admin.categories.delete', $category) }}" class="badge badge-danger fw-bold">حذف</a>--}}

                                    <a href="{{ route('admin.categories.edit', $category) }}"
                                       class="btn btn-icon btn-sm btn-clean btn-light-primary btn-active-primary"
                                       data-inbox="dismiss" data-toggle="tooltip" title="ویرایش">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                        <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-user-edit"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>


                                    <a href="{{ route('admin.categories.delete', $category) }}"
                                       class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger"
                                       data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                        <span class="svg-icon svg-icon-2 p-1">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        <!--end::Table row-->

                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                <!--end::Card body-->
            </div>
        </div>

    </div>




@endsection
