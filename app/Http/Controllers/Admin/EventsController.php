<?php

namespace App\Http\Controllers\Admin;

use App\Constants\Constant;
use App\Helpers\Format\Date;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Center;
use App\Models\Event;
use App\Models\Tag;
use Illuminate\Http\Request;

class EventsController extends BaseController
{

    public function index()
    {
        $events = Event::latest()->paginate();

//        Event::create([
//        ]);
        return view('admin.events.list', compact('events'));
    }



    public function create()
    {
        $statuses = Constant::getEventStatusesViewer();
        $centers = Center::select(['id', 'title'])->get()->toArray();
        $categories = Category::select(['id', 'title'])->get()->toArray();

        return view('admin.events.create', [
            'statuses' => $statuses,
            'centers' => $centers,
            'categories' => $categories
        ]);
    }



    public function store(Request $request)
    {
        // validation
        $request->validate([
            'title' => ['required'],
            'point' => ['required'],
            'description' => ['required'],
            'status' => ['required'],
            'image' => ['required', 'mimes:jpg,png,bmp,jpeg'],
            'start_date' => ['required'],
            'start_register_date' => ['required'],
            'end_register_date' => ['required'],
            'centers' => ['required'],
            'categories' => ['required']
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        // create event
        $data = [
            'title' => $request->input('title'),
            'point' => $request->input('point'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'start_date' => Date::toCarbonDateFormat($request->input('start_date')),
            'end_date' => Date::toCarbonDateFormat($request->input('end_date')),
            'start_register_date' => Date::toCarbonDateFormat($request->input('start_register_date')),
            'end_register_date' => Date::toCarbonDateFormat($request->input('end_register_date')),
        ];

        if ($request->file('image')) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::EVENT_IMAGE_PATH);
        }

        $event = Event::create($data);

        if ($event instanceof Event) {
            // relations : center-events
            $event->centers()->sync($request->input('centers'));

            // relations : categoriables
            $event->categories()->sync($request->input('categories'));

            // relations : taggables
            $event->tags()->sync($request->input('tags'));

            return redirect()->route('admin.events.list');
        }

        return redirect()->back();
    }



    public function edit(Event $event)
    {

        $statuses = Constant::getEventStatusesViewer();
        $centers = Center::select(['id', 'title'])->get()->toArray();
        $categories = Category::select(['id', 'title'])->get()->toArray();

        return view('admin.events.edit', [
            'event' => $event,
            'statuses' => $statuses,
            'centers' => $centers,
            'categories' => $categories
        ]);
    }



    public function update(Request $request, Event $event)
    {
        // validation
        $request->validate([
            'title' => ['required'],
            'point' => ['required'],
            'description' => ['required'],
            'status' => ['required'],
            'image' => ['nullable', 'mimes:jpg,png,bmp,jpeg'],
            'start_date' => ['required'],
            'start_register_date' => ['required'],
            'end_register_date' => ['required'],
            'centers' => ['required'],
            'categories' => ['required']
        ], [
            '*.required' => 'فیلد مورد نظر الزامی میباشد.'
        ]);

        $data = [
            'title' => $request->input('title'),
            'point' => $request->input('point'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
            'start_date' => Date::toCarbonDateFormat($request->input('start_date')),
            'end_date' => Date::toCarbonDateFormat($request->input('end_date')),
            'start_register_date' => Date::toCarbonDateFormat($request->input('start_register_date')),
            'end_register_date' => Date::toCarbonDateFormat($request->input('end_register_date')),
        ];

        if ($request->file('image')) {
            $data['image'] = $this->uploadImage($request->file('image'), Constant::EVENT_IMAGE_PATH);
        }

        $event->update($data);

        if ($event instanceof Event) {
            // relations : center-events
            $event->centers()->sync($request->input('centers'));

            // relations : categoriables
            $event->categories()->sync($request->input('categories'));

            return redirect()->route('admin.events.list');
        }

        return redirect()->back();
    }



    public function delete(Event $event)
    {
        $event->delete();

        return redirect()->back();
    }



    public function ajax_search(Request $request)
    {
        $title = $request->input('q');
        $tags = Tag::where('title', 'like', '%' . $title . '%')->get();

        $output = [];
        foreach ($tags as $tag) {
            $output[] = [
                'id' => $tag->id,
                'name' => $tag->title
            ];
        }

        return response()->json(['data' => $output]);
    }
}
