<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Web\Category\CategoryPresenter as webCategoryPresenter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory,Sluggable, Presentable;

    public $webPresenter = webCategoryPresenter::class;

    protected $guarded = ['id'];


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,
            ]
        ];
    }


    public function events()
    {
        return $this->morphedByMany(Event::class, 'categoriable');
    }


    public function blogs()
    {
        return $this->morphedByMany(Blog::class, 'categoriable');
    }
}
